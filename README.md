# Music Genres’ Effect on EEG signals and Human Mental State

- This is my Fundamentals of Neuroscience Course Project
- Teamwork with: Ali Ghavampour, Mohammad Amin Alamolhoda, Yasamin Medghalchi

- **Aim:** Investigation of the relationship between EEG signals bandpower to the genre of music playing.
- **Aim:** Investigation of the correlation between EEG signals bandpower to the human mental state.

- **Abstract:** In this paper, the relation between listening to different music genres and human mental states is investigated. The mental states which have been studied are drowsiness, focus, and anxiety. First, the paper talks about how listening to different music genres cause variations in EEG signal frequency band powers. Next, the correlation between participants mental states and the EEG signal frequency band power variations after listening to the music is analyzed. Furthermore, the participants mental states during the task are acquired by a questionnaire. EEG data acquired from subjects in ten trials of listening to five music genres with rests within. Results indicate a few minor correlations between the three stages of the analysis. Overall, the study contributes to the development of our understanding of how music genres would affect mental states with the help of analyzing EEG frequency band powers.
